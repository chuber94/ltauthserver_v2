
from rest_framework import serializers

from . import models
from . import selectors

def user_register(*, serializer: serializers.Serializer):
    user = models.User(**serializer.validated_data)

    try:
        user.full_clean()
    except Exception as e:
        raise serializers.ValidationError(e)

    user.save()

def user_delete(*, serializer: serializers.Serializer):
    user = selectors.user_list(filters={"username": serializer.validated_data['username']})
   
    if user.count() > 1:
        raise serializers.ValidationError("More then one user with this name. This sould not be possible !!!")
  
    if user.count() < 1:
        raise serializer.ValidationError("No user with this name found")

    user.delete()

def user_edit(*, serializer: serializers.Serializer):
    user = selectors.user_list(filters={"username": serializer.validated_data['username']})
   
    if user.count() > 1:
        raise serializers.ValidationError("More then one user with this name. This sould not be possible !!!")
  
    if user.count() < 1:
        raise serializers.ValidationError("No user with this name found")

    user = user.last()
    if serializer.validated_data.get("password", None) != None:
        user.password = serializer.validated_data['password']

    if serializer.validated_data.get("expire_date", None) != None:
        user.expire_date = serializer.validated_data['expire_date']
    
    try:
        user.full_clean()
    except Exception as e:
        raise serializers.ValidationError(e)

    user.save()

