from django.db import models

from django.contrib.auth.models import (
	AbstractBaseUser, BaseUserManager, PermissionsMixin)


class UserManager(BaseUserManager):

    def create_user(self, username, expire_date, password=None):
        if username is None:
            raise TypeError('Users need a username')
        
        user = self.model(username=username, expire_date=expire_date)
        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, username, password):
        if password is None:
            raise TypeError('Password should not be None')
        
        user = self.create_user(username, None, password)
        user.is_superuser = True
        user.is_staff = True
        user.save()

        return user

class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=500, unique=True, db_index=True)
    expire_date = models.DateField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    
    USERNAME_FIELD = 'username'

    #REQUIRED_FIELDS = [,]

    objects = UserManager()

    def __str__(self):
        return self.username


