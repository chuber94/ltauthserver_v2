
from . import models

def user_list(*, filters=None):
    filters = filters or {}

    qs = models.User.objects.all()
    return qs.filter(**filters)
