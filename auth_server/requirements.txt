Automat==0.6.0
PAM==0.4.2
SecretStorage==2.3.1
Twisted==17.9.0
argparse==1.2.1
asn1crypto==0.24.0
attrs==17.4.0
certifi==2018.1.18
chardet==3.0.4
click==6.7
colorama==0.3.7
constantly==15.1.0
cryptography==2.1.4
enum34==1.1.6
gyp==0.1
hyperlink==17.3.1
idna==2.6
incremental==16.10.1
ipaddress==1.0.17
keyring==10.6.0
keyrings.alt==3.0
psycopg2==2.5.2
pyOpenSSL==17.5.0
pyasn1==0.4.2
pyasn1-modules==0.2.1
pycrypto==2.6.1
pygobject==3.26.1
pyserial==3.4
python-apt==1.6.5-ubuntu0.4
python-debian==0.1.32
pyxdg==0.25
requests==2.18.4
service-identity==16.0.0
six==1.11.0
stevedore==0.13
urllib3==1.22
virtualenv==1.11.1
virtualenv-clone==0.2.4
virtualenvwrapper==4.2
wheel==0.30.0
wsgiref==0.1.2
zope.interface==4.3.2
