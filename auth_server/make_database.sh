rm db.sqlite3
rm -r auth_app/migrations/
python manage.py makemigrations
python manage.py makemigrations auth_app
python manage.py migrate
python manage.py createsuperuser --username admin
