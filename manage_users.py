
import requests
import json
from getpass import getpass

PROTOCOL = "https"

def get_token(user, password):

    response = requests.post(PROTOCOL+"://authentication.ira.uka.de/auth/mediator/",data={"name":user,"password":password})

    token = json.loads(response.text)["token"]

    return token

user = input("Input admin username: ")
password = password = getpass("Input admin password: ")
token = get_token(user,password)

while True:
    command = input("0:list users, 1:register user, 2:edit user, 3:remove user: ")

    if command=="0":
        command = "users"
    elif command=="1":
        command = "registerUser"
    elif command=="2":
        command = "editUser"
    else:
        command = "deleteUser"

    if command=="users":
        response = requests.get(PROTOCOL+"://authentication.ira.uka.de/auth/"+command+"/", headers={"Authorization":"Token "+token})
        result = json.loads(response.text)
        print([x["username"] for x in result["users"]])
    else:
        username = input("Input new username: " if command=="registerUser" else "Input username: ")
        if command!="deleteUser":
            password2 = getpass("Input new password: ")
            password3 = getpass("Repeat new password: ")
            if password2!=password3:
                print("Inputs do not match!")
                continue
            expire_date = input("Input expire date (YYYY-MM-DD) or empty for None: ")
        else:
            password2 = None
            expire_date = None

        if expire_date=="":
            expire_date=None

        response = requests.post(PROTOCOL+"://authentication.ira.uka.de/auth/"+command+"/", headers={"Authorization":"Token "+token},
                                 data={"username":username,"password":password2,"expire_date":expire_date,"is_superuser":False})
        print(response.text)

